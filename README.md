# Odin Sign-up Form

This project consists of a simple sign up form using HTML form controls and CSS. It is part of the Odin Project curriculum. The project
description can be found [here](https://www.theodinproject.com/lessons/node-path-intermediate-html-and-css-sign-up-form).

## License
This project is provided under the MIT License, copyright 2023 David Palubin.
