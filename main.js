const passwords = Array.from(document.querySelectorAll('input[type=password'));
const pword_match = document.querySelector('.pword-match');
const button = document.querySelector('button');
const inputs = Array.from(document.querySelectorAll('input'));

passwords.forEach((password)=>password.addEventListener('input', () => {
    if(passwords[0].value !== passwords[1].value){
      pword_match.textContent = 'Passwords do not match';
    } else {
      pword_match.textContent = '';
    }
    return;
}));

button.addEventListener('click', () => {
  inputs.forEach((input) => input.classList.add('validation'));
  return;
});

inputs.forEach((input) => input.addEventListener('input', () => {
  if(input.value){
    input.classList.add('user-input');
  } else {
    input.classList.remove('user-input');
  }
}));
